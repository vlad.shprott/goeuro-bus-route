package com.goeuro.bus.route.service.route;

import com.goeuro.bus.route.service.loader.BusRouteLoader;
import com.goeuro.bus.route.service.route.response.BusRouteCheckResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class BusRouteServiceImpl implements BusRouteService {
    private final BusRouteLoader busRouteLoader;

    public BusRouteServiceImpl(BusRouteLoader busRouteLoader) {
        this.busRouteLoader = busRouteLoader;
    }

    @Override
    public BusRouteCheckResponse checkDirectRoute(int departureId, int arrivalId) {
        log.info("Checking direct route. Departure id {}, arrival id {}", departureId, arrivalId);

        final boolean exist = busRouteLoader.load().stream().anyMatch(
                route -> route.getStationIds().contains(departureId) && route.getStationIds().contains(arrivalId)
        );

        log.info("Direct route exists: {}", exist);

        return new BusRouteCheckResponse(departureId, arrivalId, exist);
    }

    public static void main(String[] args) {
        for (int i = 0; i < 1002; i++) {
            System.out.print(i+2 + " ");
        }
    }
}
