package com.goeuro.bus.route.service.loader;

import com.goeuro.bus.route.domain.BusRoute;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.nio.file.Files;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.lang.Integer.parseInt;
import static java.nio.file.Paths.get;
import static java.util.Optional.empty;
import static java.util.Optional.of;

@Service
@Slf4j
public class BusRouteFileLoader implements BusRouteLoader {
    private static final int ROUTES_LIMIT = 100_000;
    private static final int STATIONS_LIMIT_PER_BUS = 1000;
    private static final int MIN_STATION_LIMIT = 3;

    private final String filePath;

    public BusRouteFileLoader(@Value("${file.path:}") String filePath) {
        this.filePath = filePath;
    }

    /**
     * Method loads bus routes lazily. <br/>
     * An implementation detail - after first successfull
     * method call all data will be cached using Spring's {@link Cacheable}
     * and next calls to method will retrieve data directly from cache.
     * If any {@link Throwable} occurs inside, data won't be loaded
     * until error resolved
     *
     * @return collection of bus routes
     */
    @Override
    @Cacheable(value = "routes", key = "#root.methodName")
    public List<BusRoute> load() {
        return loadRoutes();
    }

    @SneakyThrows
    private List<BusRoute> loadRoutes() {
        if (this.filePath == null || this.filePath.isEmpty()) {
            throw new IllegalArgumentException("Could not load routes. File path is invalid");
        }

        final List<String> lines = Files.readAllLines(get(this.filePath));

        if (lines.stream().anyMatch(this::notNumber)) {
            throw new IllegalStateException("Invalid routes format (only space separated numbers allowed)");
        }

        final int routeLimit = parseInt(lines.get(0));

        if (routeLimitOutOfRange(routeLimit)) {
            throw new IllegalStateException("Route limit exceeded. Please use valid range from 1 to 100,000");
        }

        return lines
                .stream()
                .skip(1)
                .limit(routeLimit)
                .map(this::parse)
                .filter(Optional::isPresent)
                .map(Optional::get)
                .collect(Collectors.toList());
    }

    /**
     * Method parses single line and returns parsed data
     * as an {@link Optional<BusRoute>}.
     * {@link Optional} is mainly used for <b>null safety</b> to protect method caller only
     *
     * @param line string from file
     * @return optional bus route
     */
    private Optional<BusRoute> parse(String line) {
        if (line.isEmpty()) {
            return empty();
        }

        final List<String> filtered =
                Stream
                        .of(line.trim().split("\\s+"))
                        .filter(s -> !s.isEmpty())
                        .collect(Collectors.toList());

        if (lessThanMinStationLimit(filtered.size() - 1)) {
            log.warn("Not enough stations to parse (min 3). Cannot parse bus route. Route Id:{}", filtered.get(0));
            return empty();
        }

        if (stationsLimitOutOfRange(filtered.size() - 1)) {
            log.warn("Stations limit per bus exceeded. Cannot parse bus route. Route Id:{}", filtered.get(0));
            return empty();
        }

        return of(
                new BusRoute(
                        parseInt(filtered.get(0)),
                        filtered.stream().skip(1).map(Integer::parseInt).collect(Collectors.toSet())
                )
        );
    }

    private boolean notNumber(String string) {
        return string != null && !string.matches("^[0-9 ]+$");
    }

    private boolean routeLimitOutOfRange(int limit) {
        return limit <= 0 || limit > ROUTES_LIMIT;
    }

    private boolean lessThanMinStationLimit(int total) {
        return total < MIN_STATION_LIMIT;
    }

    private boolean stationsLimitOutOfRange(int limit) {
        return limit > STATIONS_LIMIT_PER_BUS;
    }
}
