package com.goeuro.bus.route.service.route.response;

import com.fasterxml.jackson.annotation.JsonProperty;

public class BusRouteCheckResponse {

    @JsonProperty("dep_sid")
    private final int departureId;

    @JsonProperty("arr_sid")
    private final int arrivalId;

    @JsonProperty("direct_bus_route")
    private final boolean directBusRoute;

    public BusRouteCheckResponse(int departureId, int arrivalId, boolean directBusRoute) {
        this.departureId = departureId;
        this.arrivalId = arrivalId;
        this.directBusRoute = directBusRoute;
    }

    public int getDepartureId() {
        return departureId;
    }

    public int getArrivalId() {
        return arrivalId;
    }

    public boolean isDirectBusRoute() {
        return directBusRoute;
    }
}
