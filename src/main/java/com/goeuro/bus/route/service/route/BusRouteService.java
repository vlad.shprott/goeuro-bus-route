package com.goeuro.bus.route.service.route;

import com.goeuro.bus.route.service.route.response.BusRouteCheckResponse;

public interface BusRouteService {
    BusRouteCheckResponse checkDirectRoute(int departureId, int arrivalId);
}
