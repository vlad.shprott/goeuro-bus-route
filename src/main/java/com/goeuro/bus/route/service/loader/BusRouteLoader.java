package com.goeuro.bus.route.service.loader;

import com.goeuro.bus.route.domain.BusRoute;

import java.util.List;

public interface BusRouteLoader {
    List<BusRoute> load();
}
