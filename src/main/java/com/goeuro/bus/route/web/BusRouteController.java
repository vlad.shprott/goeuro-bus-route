package com.goeuro.bus.route.web;

import com.goeuro.bus.route.service.route.BusRouteService;
import com.goeuro.bus.route.service.route.response.BusRouteCheckResponse;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class BusRouteController {

    private final BusRouteService busRouteService;

    public BusRouteController(BusRouteService busRouteService) {
        this.busRouteService = busRouteService;
    }

    @GetMapping("/api/direct")
    public BusRouteCheckResponse checkDirectRoute(@RequestParam("dep_sid") int departureId, @RequestParam("arr_sid") int arrivalId) {
        return busRouteService.checkDirectRoute(departureId, arrivalId);
    }
}
