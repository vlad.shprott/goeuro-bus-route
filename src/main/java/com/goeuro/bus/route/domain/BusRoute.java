package com.goeuro.bus.route.domain;

import lombok.Data;

import java.util.Set;

@Data
public class BusRoute {

    private final int id;

    private final Set<Integer> stationIds;
}
