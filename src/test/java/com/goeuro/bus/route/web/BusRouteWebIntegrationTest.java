package com.goeuro.bus.route.web;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@RunWith(SpringRunner.class)
@TestPropertySource(properties = {"file.path = data/example"})
public class BusRouteWebIntegrationTest {

    @Autowired
    private MockMvc mvc;

    @Test
    public void testGetWithoutRequiredParams() throws Exception {
        mvc.perform(
                get("/api/direct")
        ).andExpect(status().isBadRequest());
    }

    @Test
    public void testPostNotAllowed() throws Exception {
        mvc.perform(
                post("/api/direct")
        ).andExpect(status().isMethodNotAllowed());
    }

    @Test
    public void testGetDirectRouteExists() throws Exception {
        mvc.perform(
                get("/api/direct")
                        .param("dep_sid", "153")
                        .param("arr_sid", "150")
        )
                .andExpect(status().isOk())
                .andExpect(jsonPath("dep_sid").value(153))
                .andExpect(jsonPath("arr_sid").value(150))
                .andExpect(jsonPath("direct_bus_route").value(true));
    }

    @Test
    public void testGetDirectRouteDoestNotExist() throws Exception {
        mvc.perform(
                get("/api/direct")
                        .param("dep_sid", "1")
                        .param("arr_sid", "1")
        )
                .andExpect(status().isOk())
                .andExpect(jsonPath("dep_sid").value(1))
                .andExpect(jsonPath("arr_sid").value(1))
                .andExpect(jsonPath("direct_bus_route").value(false));
    }

}