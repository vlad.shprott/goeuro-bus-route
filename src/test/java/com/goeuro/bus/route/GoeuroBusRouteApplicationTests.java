package com.goeuro.bus.route;

import com.goeuro.bus.route.service.loader.BusRouteLoader;
import com.goeuro.bus.route.service.route.BusRouteService;
import com.goeuro.bus.route.web.BusRouteController;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertNotNull;

@RunWith(SpringRunner.class)
@SpringBootTest
public class GoeuroBusRouteApplicationTests {
	@Autowired
	private BusRouteLoader loader;

	@Autowired
	private BusRouteService service;

	@Autowired
	private BusRouteController controller;

	@Test
	public void contextLoads() {
		assertNotNull(loader);
		assertNotNull(service);
		assertNotNull(controller);
	}

}

