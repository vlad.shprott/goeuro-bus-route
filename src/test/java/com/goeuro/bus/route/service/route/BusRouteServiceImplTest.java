package com.goeuro.bus.route.service.route;

import com.goeuro.bus.route.domain.BusRoute;
import com.goeuro.bus.route.service.loader.BusRouteLoader;
import com.goeuro.bus.route.service.route.response.BusRouteCheckResponse;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.internal.util.collections.Sets;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;

import static java.util.Collections.singletonList;
import static org.junit.Assert.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@SpringBootTest
@RunWith(SpringRunner.class)
public class BusRouteServiceImplTest {

    @Autowired
    private BusRouteService busRouteService;

    @MockBean
    private BusRouteLoader busRouteLoader;

    @Test
    public void checkDirectRouteExist() {
        //given
        final BusRoute single = new BusRoute(1, Sets.newSet(1, 2, 3, 4, 5));
        when(busRouteLoader.load()).thenReturn(singletonList(single));

        final int departureId = 2;
        final int arrivalId = 5;
        //when
        final BusRouteCheckResponse response =
                busRouteService.checkDirectRoute(departureId, arrivalId);
        //then
        verify(busRouteLoader).load();
        assertNotNull(response);
        assertEquals(response.getDepartureId(), departureId);
        assertEquals(response.getArrivalId(), arrivalId);
        assertTrue(response.isDirectBusRoute());
    }

    @Test
    public void checkDirectRouteNotExist() {
        //given
        final BusRoute single = new BusRoute(1, Sets.newSet(1, 2, 3, 4, 5));
        when(busRouteLoader.load()).thenReturn(singletonList(single));

        final int departureId = 1;
        final int arrivalId = 6;
        //when
        final BusRouteCheckResponse response =
                busRouteService.checkDirectRoute(departureId, arrivalId);
        //then
        verify(busRouteLoader).load();
        assertNotNull(response);
        assertEquals(response.getDepartureId(), departureId);
        assertEquals(response.getArrivalId(), arrivalId);
        assertFalse(response.isDirectBusRoute());
    }

}