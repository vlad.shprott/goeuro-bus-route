package com.goeuro.bus.route.service.loader;

import com.goeuro.bus.route.domain.BusRoute;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.ResourceUtils;

import java.io.FileNotFoundException;
import java.util.List;
import java.util.Objects;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.springframework.test.util.ReflectionTestUtils.setField;

@SpringBootTest
@RunWith(SpringRunner.class)
public class BusRouteFileLoaderTest {

    @Autowired
    private BusRouteLoader busRouteLoader;

    @Autowired
    private CacheManager cacheManager;

    @Before
    public void before() {
        cacheManager.getCacheNames()
                .stream()
                .map(cacheManager::getCache)
                .filter(Objects::nonNull)
                .forEach(Cache::clear);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testLoadWithInvalidFilePath() {
        //given
        setField(busRouteLoader, "filePath", null);
        //when
        busRouteLoader.load();
        //then exceptionally
    }

    @Test
    public void testLoadWithValidFile() {
        //given
        setField(busRouteLoader, "filePath", path("data/valid"));
        //when
        final List<BusRoute> routes = busRouteLoader.load();
        //then
        assertNotNull(routes);
        assertEquals(routes.size(), 10);
    }

    @Test(expected = IllegalStateException.class)
    public void testLoadWithInvalidRouteLimit() {
        //given
        setField(busRouteLoader, "filePath", path("data/invalidRouteLimit"));
        //when
        busRouteLoader.load();
        //then exceptionally
    }

    @Test(expected = IllegalStateException.class)
    public void testLoadWithInvalidRouteFormat() {
        //given
        setField(busRouteLoader, "filePath", path("data/invalidRouteFormat"));
        //when
        busRouteLoader.load();
        //then exceptionally
    }

    @Test
    public void testLoadWithNotEnoughStations() {
        //given
        setField(busRouteLoader, "filePath", path("data/notEnoughStations"));
        //when
        final List<BusRoute> routes = busRouteLoader.load();
        //then
        assertNotNull(routes);
        assertEquals(routes.size(), 9);
    }

    @Test
    public void testLoadWithStationsPerRouteLimitExceeded() {
        //given
        setField(busRouteLoader, "filePath", path("data/invalidStationLimit"));
        //when
        final List<BusRoute> routes = busRouteLoader.load();
        //then
        assertNotNull(routes);
        assertEquals(routes.size(), 0);
    }

    private String path(String path) {
        try {
            return ResourceUtils.getFile(this.getClass().getClassLoader().getResource(path)).getAbsolutePath();
        } catch (FileNotFoundException e) {
            Assert.fail("File not found");
        }
        return "";
    }
}